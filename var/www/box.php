<?php 

#####################################################
# MODULE:   box.php
# PURPOSE:  this is the main interface
# USAGE:    point, click, drag etc.
# USED BY:  private < api.php
# LICENSE:  MIT or equivalent
# AUTHOR:   denjell
# VERSION:  0.6.1
# PROJECT:  ARTBOX.IO
#####################################################

?>

<div class="container-fluid">
  <div class="row-fluid">
    <div class="span7">
      <div class="accordion"  id="accordion2">

<div class="accordion-group">
	<div  class="accordion-heading"> 
		<div id="scheduleToggle" class="accordion-toggle " data-toggle="collapse" data-parent="#accordion2" href="#collapse2">
			<i class="icon-time"></i> SCHEDULE
		</div>

	</div>
<div id="collapse2" class="accordion-body collapse schedule">
<div class="accordion-inner" style="min-height:10em">
<div class="dropdown"> 
  <span class="dropdown-toggle btn btn-mini" id="dLabel" role="menu" data-toggle="dropdown">
    <span id="scheduleType"><span class="scheduleIcon">?</span> - <span class="scheduleLabel">Schedule Type</span></span>
     <b class="caret"></b>
  </span>
   <ul class="dropdown-menu scheduler" aria-labelledby="dLabel">
    <li class="Infiniteloop"><a class="Infiniteloop"><span class="scheduleIcon">&#8734;</span> - <span class="scheduleLabel">Infiniteloop</span></a></li>
    <li class="Daily"><a class="Daily"><span class="scheduleIcon">1x</span> - <span class="scheduleLabel">Daily</span></a></li>

 <!-- TURNED OFF UNTIL THE CRONTABS ARE VETTTED
    <li class="Weekly"><a class="Weekly"><span class="scheduleIcon">7x</span> - <span class="scheduleLabel">Weekly</span></a></li>
    <li class="Special"><a class="Special"><span class="scheduleIcon">!!!</span> - <span class="scheduleLabel">Special</span></a></li>
 -->
    <li class="Range"><a class="Special"><span class="scheduleIcon">><</span> - <span class="scheduleLabel">Range</span></a></li>
  </ul>  
  <i class="icon-info-sign" onclick="$('.infoSchedule').toggleClass('hidden')"></i>
  </div>






<div class="schedules">
<div class="hidden interface2 Infiniteloop" data-type="Infiniteloop">  
  <table class="scheduler" data-provides="rowlink">
    <thead><tr><td>Day</td><td class="on">Start</td><td class="off">Stop</td></tr></thead>
    <tr class="Everday"><td class="nolink Daily">ALL</td><td class="nolink on" ><a href="#">NOW</a></td><td class="nolink off">FALSE</td></tr>
  </table>
    <span class="infoSchedule hidden">

  This playlist schedule will run your Artbox each and every day and night until the Sun supernovas or you publish a new playlist.
</span>


</div>
<div class=" hidden interface2 Daily" data-type="Daily">
  <table class="scheduler">
    <thead><tr><td>Day</td><td class="on">Start</td><td class="off">
<span class="dropdown"> 
  <span class="dropdown-toggle stopper" id="dLabel2" role="menu"  data-toggle="dropdown">
    <span class="stopType"><span>Stop</span></span>
     <b class="caret"></b>
    </span>
   <ul class="dropdown-menu stopper" aria-labelledby="dLabel2">
    <li class="Stop stoptype"><a class="Stop"><img src="<?php print constant("SYSURL") ?>/images/clock_stop.png"> <span>Stop</span></a></li>
    <li class="Length stoptype"><a class="Length"><img src="<?php print constant("SYSURL") ?>/images/progressbar.png"> <span>Length</span></a></li>
    <li class="Loops stoptype"><a class="Loops"><img src="<?php print constant("SYSURL") ?>/images/counter.png"> <span>Loops</span></a></li>
  </ul>
</span>
    </td></tr></thead>
    <tr class="EACH"><td class="EACH"><a class="scheduler minicon"><img class="EXdelete" src="images/delete.png"></a>ALL</td><td class="on"><input class="timePick on"/></td></td><td class="nolink off"><input class="off timePick Stop hidden"/></td></tr>

  </table>
  <span class="infoSchedule hidden">
  This type of schedule will play every day from a start time until a stop time. You can set the stop time to be a time of day, a length of time or a number of loops.
  </span> 

</div>
<!--
<div class="hidden interface2 Weekly" data-type="Weekly">

  <table class="scheduler">
    <thead><tr><td>Day</td><td class="on">Start</td><td class="nolink off">
<span class="dropdown"> 
  <span class="dropdown-toggle stopper" id="dLabel2" role="menu"  data-toggle="dropdown">
    <span class="stopType"><span>Stop</span></span>
     <b class="caret"></b>
    </span>
   <ul class="dropdown-menu stopper" aria-labelledby="dLabel2">
    <li class="Stop stoptype"><a class="Stop"><img src="<?php print constant("SYSURL") ?>/images/clock_stop.png"> <span>Stop</span></a></li>
    <li class="Length stoptype"><a class="Length"><img src="<?php print constant("SYSURL") ?>/images/progressbar.png"> <span>Length</span></a></li>
    <li class="Loops stoptype"><a class="Loops"><img src="<?php print constant("SYSURL") ?>/images/counter.png"> <span>Loops</span></a></li>
  </ul>
</span>
    </td></tr></thead>
    <tr class="Monday">
    <td class="Monday"><a class="scheduler minicon"><img class="EXdelete" src="images/delete.png"></a> <input value="Monday" disabled></td><td class="on"><input class="timePick on"/></td></td><td class="nolink off"><input class="off timePick Stop hidden"/></td></tr>
    <tr class="Tuesday">
      <td class="Tuesday"><a class="scheduler minicon"><img class="EXdelete" src="images/delete.png"></a> <input value="Tuesday" disabled></td><td class="on"><input class="on timePick"/></td><td class="nolink off"><input class="off timePick Stop hidden"/></td></tr>
    <tr class="Wednesday">
      <td class="Wednesday"><a class="scheduler minicon"><img class="EXdelete" src="images/delete.png"></a> <input value="Wednesday" disabled></td><td class="on"><input class="on timePick"/></td><td class="nolink off"><input class="off timePick Stop hidden"/></td></tr>
    <tr class="Thursday">
      <td class="Thursday"><a class="scheduler minicon"><img class="EXdelete" src="images/delete.png"></a> <input value="Thursday" disabled></td><td class="on"><input class="on timePick"/></td><td class="nolink off"><input class="off timePick Stop hidden"/></td></tr>
    <tr class="Friday">
      <td class="Friday"><a class="scheduler minicon"><img class="EXdelete" src="images/delete.png"></a> <input value="Friday" disabled></td><td class="on"><input class="on timePick"/></td><td class="nolink off"><input class="off timePick Stop hidden"/></td></tr>
    <tr class="Saturday">
      <td class="Saturday"><a class="scheduler minicon"><img class="EXdelete" src="images/delete.png"></a> <input value="Saturday" disabled></td><td class="on"><input class="on timePick"/></td><td class="nolink off"><input class="off timePick Stop hidden"/></td></tr>
    <tr class="Sunday">
      <td class="Sunday"><a class="scheduler minicon"><img class="EXdelete" src="images/delete.png"></a> <input value="Sunday" disabled></td><td class="on"><input class="on timePick"/></td><td class="nolink off"><input class="off timePick Stop hidden"/></td></tr>
  </table>
  <span class="infoSchedule hidden">
  This type of schedule lets you set start and end times for each day of the week. You can set the stop time to be a time of day, a length of time or a number of loops.
  </span> 

</div>
<div class=" hidden interface2 Special" data-type="Special">
  <table  class="scheduler">
    <thead><tr><td>Day</td><td class="on">Start</td><td class="off">
<span class="dropdown"> 
  <span class="dropdown-toggle stopper" id="dLabel2" role="menu"  data-toggle="dropdown">
    <span class="stopType"><span>Stop</span></span>
     <b class="caret"></b>
    </span>
   <ul class="dropdown-menu stopper" aria-labelledby="dLabel2">
    <li class="Stop stoptype"><a class="Stop"><img src="<?php print constant("SYSURL") ?>/images/clock_stop.png"> <span>Stop</span></a></li>
    <li class="Length stoptype"><a class="Length"><img src="<?php print constant("SYSURL") ?>/images/progressbar.png"> <span>Length</span></a></li>
    <li class="Loops stoptype"><a class="Loops"><img src="<?php print constant("SYSURL") ?>/images/counter.png"> <span>Loops</span></a></li>
  </ul>
</span>
    </td></tr></thead>
      <a href="#placeTime"><tr class="Date"><td class="Date"><input class="datePick"/></td><td class="on"><input class="on timePick"/></td><td class="nolink off hidden"><input class="off timePick Stop"/></td></tr></a>
  </table>
    <span class="infoSchedule hidden">
    This type of schedule lets you set a specific time and date for the playlist. You can use it for example to run for a long number of minutes such as 6000 (100 hours)
  </span>

    
</div>
-->
<div class=" hidden interface2 Range" data-type="Range">
  <table  class="scheduler">
    <thead><tr><td class="on">Start</td><td class="off">Stop</td></tr></thead>
    <tr class="Date"><td class="on"><a class="scheduler minicon"><img class="EXdelete" src="images/delete.png"></a> <input class="timePick Range"/></td><td class="nolink off"><input class="timePick Range off hidden"/></td></tr>
  </table>
  <span class="infoSchedule hidden">
  This type of schedule lets you set a long playlist that may span several days, weeks, months or years. Wow. That's a lot of art!!!
  </span>


</div>
  <!--<label class="checkbox" for="Override">
    <input id="Override" class="check disabled" name="Overide" type="checkbox" checked>Override
  </label>
  <label class="checkbox" for="Interrupt">
    <input id="Interrupt" class="check disabled" name="Interrupt" type="checkbox">Interrupt
  </label>
-->
<form id="scheduleTarget" class="hidden">
  Save Schedule to:
<label class="checkbox inline" for="Artbox">
  <input id="Artbox" class="radio" data-target="Artbox" name="scheduleTarget" type="radio" checked>Artbox
</label>

<!-- TO DO: SOLVE WRITING TO USB FROM THE BROWSER
<label class="checkbox inline" for="USB">
  <input id="USB" class="radio disabled" data-target="USB" name="scheduleTarget" type="radio">USB
</label>
-->
</form>
</div> <!-- end schedules -->
</div>
</div>
</div>


<div class="accordion-group">
  <div  class="accordion-heading"> 
    <span class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1">
      <i class="icon-list"></i> PLAYLIST
    </span>
  </div>
  <div id="collapse1" class="accordion-body collapse playlist">
    <div class="accordion-inner">
      <textarea id="box1" class="editable btn btn-mini" rows="1">First List</textarea>
      <div id="cart">
        <div class="holder"> 
          <ol id="sortable">
            <li class="placeholder">Drag Media Here</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
</div>




    <div class="accordion-group">
      <div  class="accordion-heading"> 
        <div id="scheduleToggle" class="accordion-toggle " data-toggle="collapse" data-parent="#accordion1" href="#collapse3">
        # CONSOLE
        </div>

      </div>
      <div id="collapse3" class="accordion-body collapse console in">
        <div class="accordion-inner console">
          <div class="navbar">
          <a id="activatePlaylist" class="btn btn-mini">
            <i class="icon-check"></i> Publish
            <span id="itemcount" class="label label-inverse playlist" >0</span>
            <span id="scheduleTypeLabel" class="label label-inverse playlist">?</span>
          </a>

<span class="dropdown consoleButtons"> 
            <span class="dropdown-toggle" id="dLabel3" role="menu"  data-toggle="dropdown">
              <a class="terminalButton btn btn-mini">
                <i class="icon-cog"></i> Config
              </a>
            </span>

          <ul class="dropdown-menu pull-left" aria-labelledby="dLabel3">
            <li>
              <a class="terminalButton" onclick="bash('sync',1,'Press Enter to sync the Artbox with your browser time.')">
                Sync Clocks
              </a>
            </li>
            <li class="dropdown-submenu">
              <a class="terminalButton">
                Artbox Network
              </a>
              <ul class="dropdown-menu">
                    <li><a onclick="bash('network router',0,'Pressing Enter will restart the Artbox.')"> DHCP Router</a>
                    </li>
                    <li><a onclick="bash('network client',0,'Pressing Enter will restart the Artbox.')"> DHCP Client</a>
                    </li>
                    <li class="disabled"><a onclick="bash('network client',0,'Pressing Enter will restart the Artbox.')"> WIFI SETTINGS</a>
                    </li>
                  </ul>
            </li>
            <li class="dropdown-submenu">
              <a class="terminalButton">
                Screen
              </a>
              <ul class="dropdown-menu">
                    <li class="dropdown-submenu"><a > HDMI</a>
                      <ul class="dropdown-menu">
                       <li><a onclick="bash('hdmi on',1,'')"> HDMI ON</a>
                       </li>
                        <li><a onclick="bash('hdmi off',1,'')"> HDMI OFF</a>
                        </li>
                       <li><a onclick="bash('hdmi modes',1,'')"> HDMI MODES</a>
                       </li>
                       <li><a onclick="bash('hdmi ninja',1,'')"> HDMI NINJA</a>
                       </li>
                      </ul>
                    </li>
                    <li class="dropdown-submenu"><a> COMPOSITE</a>
                      <ul class="dropdown-menu">
                        <li>
                          <a onclick="bash('network client',0,'Pressing Enter will restart the Artbox.')">COMPOSITE NTSC</a>
                      </li>
                        <li>
                          <a onclick="bash('network client',0,'Pressing Enter will restart the Artbox.')">COMPOSITE PAL</a>
                      </li>
                    </ul>
                  </li>
                  </ul>
            </li>
            <li>
              <input id="newPassword" name="newPassword" type="text" value="Change Password" style="-webkit-outline:none" min-length="8" placeholder="Change Password">
            </li>
          </ul>
        </span>        

        <span class="dropdown consoleButtons"> 
            <span class="dropdown-toggle" id="dLabel4" role="menu"  data-toggle="dropdown">
              <a class="terminalButton btn btn-mini">
                <i class="icon-off"></i> System
              </a>
            </span>

          <ul class="dropdown-menu pull-left" aria-labelledby="dLabel4">
            <li>
              <a class="terminalButton" onclick="bash('halt',0,'Press Enter to immediately halt playback.')">
                Halt Playback
              </a>
            </li>      
            <li>
              <a class="terminalButton" onclick="bash('restart',0,'Press Enter to Restart.')">
                Restart Now
              </a>
            </li>
            <li>
              <a class="terminalButton" onclick="bash('shutdown',0,'Press Enter to Shutdown now.')">
                Power Off
              </a>
            </li>
           <!-- <li>
              <a class="terminalButton" onclick="bash('purge playlists',1,'')">
                Clear Playlists
              </a>
            </li>
            <li>
              <a class="terminalButton" data-toggle="modal" data-target="#modal" href="<?php print constant("SYSURL"); ?>/Upload">
                Upload
              </a>
            </li>
            <li class="disabled">
              <a class="terminalButton">
                Overclocking
              </a>
            </li>-->
            <li>
              <a class="terminalButton" onclick="bash('update',0,'Press Enter to Update your Artbox now.<br>Your connection will freeze for several minutes.')">
                Update
              </a>
            </li>
	     <li>
              <a class="terminalButton" id="suButton" onclick="suDo()">
                Superuser <span class="label label-inverse">off</span>
              </a>
	      <input id="su" name="su" type="hidden" value="off">
            </li>
          </ul>
        </span>

          <span class="dropdown consoleButtons">
            <span class="dropdown-toggle" id="dLabel5" role="menu"  data-toggle="dropdown">
              <a class="terminalButton btn btn-mini">
                <i class="icon-info-sign"></i> Help
              </a>
            </span>

          <ul class="dropdown-menu pull-left" aria-labelledby="dLabel5">
            <li>
              <a class="terminalButton" data-toggle="modal" data-target="#modal" href="<?php print constant("SYSURL"); ?>/Manual">
                Handbook
              </a>            
            </li>
            <li>
              <a class="terminalButton" onclick="bash('versions',1,'')">
                Version History
              </a>            
            </li>           
          </ul>
          </span>


<span id="fifobuttons disabled">
<a id="ffbutton" href="#" class="right playing btn btn-mini " onclick="bash('next',1,'Next chapter / clip.')"><i id="" class=" icon-fast-forward"></i></a>
<a id="pbutton" href="#" class="right playing btn btn-mini btn-success" onclick="bash('playpause',1,'Play / Pause current video.')"><i class="icon-pause"></i><i class="icon-play"></i></a>
<a id="backbutton" href="#" class="right playing btn btn-mini" onclick="bash('back',1,'Rewind to beginning of this chapter / clip.')"><i id="" class=" icon-backward"></i></a>
<a id="volupbutton" href="#" class="right playing btn btn-mini" onclick="bash('volup',1,'Increase Volume.')"><i id="" class="icon-volume-up"></i></a>
<a id="voldownbutton" href="#" class="right playing btn btn-mini" onclick="bash('voldn',1,'Decrease Volume.')"><i id="" class="icon-volume-down"></i></a>
</span>
        </div>
<div class="dark pad1 coffee roundedBottom">

<div id="messages" class="coffee hidden"><span></span>
</div>
<div id="timeNow" class="coffee">
  <span class="coffee time">
    <span id="artboxLabel">ARTBOX&nbsp;&nbsp;<span id="version">v.<?php print $version ?></span><br>
	<span id="serverTime"></span><br>
    <span id="browserLabel"></span><span id="userStatus"></span><br><span id="browserTime"></span>
  </span>
</div>
<input class="coffee" id="terminal" type="textarea">
</div>

<span class="terminalResponse"></span>
        </div>
      </div>
    </div>


</div>
</div> <!-- end accordion -->


<h4 id="noscript">
  In order to use this web interface for the Artbox you must enable Javascript und use an HTML5 compliant browser. At the moment the only browser capable of playing back all media is Google Chrome. It is a <a href="https://www.google.com/intl/en/chrome/browser/">free download</a> and is standards compliant. Welcome to the future!
</h4>



<div class="accordion" id="accordion1" >
  <div class="span5">
    <div class="accordion-group"  style="position:fixed;">
      <div  class="accordion-heading" style=""> 
        <div id="scheduleToggle" class="accordion-toggle " data-toggle="collapse" data-parent="#accordion1" style="border:none!important;" href="#collapse0">
        <i class="icon-film"></i> MEDIA

        </div>

      </div>
      <div id="collapse0" class="accordion-body" >
        <div class="accordion-inner " style="border:none;background:transparent;position:fixed;overflow-x:hidden;overflow-y:auto;max-height:70%;height:70%;min-height:70%;min-width:35%;width:35%;max-width:35%;float:right;padding:0!important">
          <div id="browser" style="background:none!important;">
            <div>
              <div id="fileTree" style="background:none!important;">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
