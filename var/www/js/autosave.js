// autosave.js
// this file contains everything for contenteditable and
// related autosaving


// http://stackoverflow.com/questions/8335834/how-can-i-hide-the-android-keyboard-using-javascript
// http://www.sencha.com/forum/archive/index.php/t-141560.html?s=6853848ccba8060cc90a9cacf7f0cb44
// not currently being used.
function hideKeyboard(element) {
    element.attr('readonly', 'readonly'); // Force keyboard to hide on input field.
    element.attr('disabled', 'true'); // Force keyboard to hide on textarea field.
    setTimeout(function() {
        element.blur();  //actually close the keyboard
        // Remove readonly attribute after keyboard is hidden.
        element.removeAttr('readonly');
        element.removeAttr('disabled');
    }, 100);
}

function  wysiwig(action){
    document.execCommand(action, false, null);
};

function doGetCaretPosition (ctrl) {

  var CaretPos = 0;
  // IE Support
  if (document.selection) {

    ctrl.focus ();
    var Sel = document.selection.createRange ();

    Sel.moveStart ('character', -ctrl.value.length);

    CaretPos = Sel.text.length;
  }
  // Firefox support
  else if (ctrl.selectionStart || ctrl.selectionStart == '0')
    CaretPos = ctrl.selectionStart;

  return (CaretPos);

}

// caret position library from 
// http://blog.vishalon.net/index.php/javascript-getting-and-setting-caret-position-in-textarea/
function setCaretPosition(beg,pos)
{
//var ctrl = $('#Volltext');

var ctrl = document.getElementById('Volltext');

  if(ctrl.setSelectionRange)
  {
    ctrl.focus();
    ctrl.setSelectionRange(beg,pos);
  }
  else if (ctrl.createTextRange) {
    var range = ctrl.createTextRange();
    range.collapse(true);
    range.moveEnd('character', pos);
    range.moveStart('character', beg);
    range.select();
  }
}

function process()
{
  var no = document.getElementById('no').value;
  setCaretPosition(document.getElementById('get'),no);
}


function autosave(content,thisID){ //autosave heavy lifting
  //console.log(thisID);
  var dataId = $('#'+thisID).attr('data-id');
  var dataKey = $('#'+thisID).attr('data-key');
  var action = "update";
  //console.log("edit:"+target);
        $.post('database.php',{
                dataId:dataId,
                dataKey:dataKey,
                content:content,
                action:action
         },
         function(data){
           console.log(data);
           if (data == "") {data="No connection to database.";}
           message(data,"#profileCardAlert", 2000,"error");

         });
  
    //  data-id="1" data-column="details" data-language="en" 
}

$(function(){

// this prevents the user from typing enter on the one-liners
$("span[contenteditable]").on("keydown", function(e) {
	    if (e.keyCode == 13) {
	        e.preventDefault();
	    }
});
$("[contenteditable]").on("input", function() {
    autosave($(this).html(),this.id);
    console.log($(this).html());
})
.bind('paste',function(e){

// we want the user to purposefully turn on html pasting, because otherwise it defeats the purpose
// because no one will remember to do it otherwise. 
  if ($('#paste>strong').html() == "off") { 

    var rte = $(this);
    var _activeRTEData = $(rte).html();
    beginLen = $.trim($(rte).html()).length; 

    setTimeout(function(){
        var text = $(rte).html();
        var newLen = $.trim(text).length;

        //identify the first char that changed to determine caret location
        caret = 0;

        for(i=0;i < newLen; i++){
            if(_activeRTEData[i] != text[i]){
                caret = i-1;
                break;  
            }
        }

        var origText = text.slice(0,caret);
        var newText = text.slice(caret, newLen - beginLen + caret + 4);
        var tailText = text.slice(newLen - beginLen + caret + 4, newLen);

        // added
        var caretPosEnd = origText.length +++ newText.length;
         console.log("origText:"+origText.length+"\nnewText:"+newText.length+"\ntailText:"+tailText.length+"\nnewLen:"+newLen);
         console.log("caretPos:"+caretPosEnd);
        var newText = newText.replace(/(.*(?:endif-->))|([ ]?<[^>]*>[ ]?)|(&nbsp;)|([^}]*})/g,'');

        newText = newText.replace(/[·]/g,'');
        var caretPos2 = origText + newText;
        $(rte).html(origText + newText + tailText);

        setCaretPosition(origText.length, caretPosEnd);

    },100);
  }
})
.bind('dragover drop', function(event){
    event.preventDefault();
    return false;
});



// Count the characters left. Should functionalize.
  $('input#newsTitle').keyup(function () {
  	console.log("presse");
    var label = 'label[for='+this.id+'] span';
    var max = $(this).attr("maxlength");
    if (!max) { // leave early if no maxlength is set
      return;
    }

    var len = $(this).val().length;

/* -------------------------------------> seems this isn't necessary after all. (or maybe only on chrome)
    // count the f*ing line breaks
    var text = $(this).val();
    // look for any "\n" occurences
    var matches = text.match(/\n/g);
    // count them, if there are any
    var breaks = matches ? matches.length : 0;
    len = len - breaks;
*/
    if (len >= max) {
      $(label).text('0 Chars left.').addClass('red').removeClass('green');
    } else {
      var char = max - len;
      $(label).text(char + ' Chars left.').addClass('green').removeClass('red');
    }
  });
$(['contenteditable']).each(function(){
var emp=$(this).html();
//console.log('1');
if (!emp) {
	$(this).addClass('hidden');
}

})


})