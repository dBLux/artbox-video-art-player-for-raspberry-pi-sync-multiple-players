<?php 
define('SYSPATH',"/var/www");
define('SYSURL',"https://".$_SERVER['SERVER_NAME']);
?>


function suDo(){
    var status=$("#suButton span"), userStatus=$("#userStatus");
    if (status.text()=="off") {
	status.text("on");
	userStatus.html("&nbsp;&nbsp; - <strong>SUPERUSER</strong>")
    } else {
	status.text("off");
	userStatus.html("");
    }

}
// these makers should be combined into one function!

function iconMaker(iconset, icon){
    icon = (typeof icon == "undefined")?'nothing':icon;
    $('#'+iconset).css('background-image','url(<?php print constant("SYSURL") ?>/images/'+icon+'.png)');
}
function badgeMaker(iconset, badge){
      badge = (typeof badge == "undefined")?'nothing':badge; 
      $('#'+iconset).attr('src','<?php print constant("SYSURL") ?>/images/overlay_'+badge+'.png');
}
function miniMaker(iconset, mini){
      mini = (typeof mini == "undefined")?'nothing':mini;    
      $('#'+iconset).attr('src','<?php print constant("SYSURL") ?>/images/'+mini+'.png');
}
function extensionMaker(iconset, icon){
    icon = (typeof icon == "undefined")?'nothing':icon;
    $('#'+iconset).attr('src','<?php print constant("SYSURL") ?>/images/file_extension_'+icon+'.png');
}
function buttonbar(holder, name){
  var buttons='<span id="'+name+'" class="buttonbar"><a href="#delete:'+holder+'" class="minicon delete "><img class="EXdelete" src="images/delete.png"></a><a href="#info:'+holder+'" class="minicon info" "><img src="images/zoom.png"></a></span>';
  //console.log(buttons + " | "+holder);
  return buttons;
}
function listCount(val){
  $('#itemcount').html($('#sortable li').length-val);
}

function activatebuttonbar(){
  $(".delete").unbind().bind({'click': function(){
      $(this).parent("span").parent('li').effect("fade", {}, 500, function(){
          $(this).remove();
          listCount(0);
      });
    }   
  });
  $('.info').unbind().bind({'click': function(){
    $(this).parent('span').parent('li').find('a.file').click()
    }
  })
  listCount(0);
}

function addToCart(event, ui ){
  var buttons=buttonbar(this);
  $( this ).find( ".placeholder" ).remove();
  $('.buttons').removeClass('hidden');
  $( "<li></li>" )
  .html( ui + buttons )
  .appendTo( this )
  .addClass("cart-item"); 
}


function showPlaylist(){
 $('.accordion-body.playlist').each(function(){
    if ($(this).hasClass('in')) {
      $('.accordion-body.schedule').collapse('hide');
      $('.accordion-body.console').collapse('hide');
    } else {
      $('.accordion-body.schedule').collapse('hide');
      $('.accordion-body.console').collapse('hide');
      $('.accordion-body.playlist').collapse('show');
    }
  })
}



var browserCheck = {
// SRC -> http://www.quirksmode.org/js/detect.html
  init: function () {
    this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
    this.version = this.searchVersion(navigator.userAgent)
      || this.searchVersion(navigator.appVersion)
      || "an unknown version";
    this.OS = this.searchString(this.dataOS) || "an unknown OS";
  },
  searchString: function (data) {
    for (var i=0;i<data.length;i++) {
      var dataString = data[i].string;
      var dataProp = data[i].prop;
      this.versionSearchString = data[i].versionSearch || data[i].identity;
      if (dataString) {
        if (dataString.indexOf(data[i].subString) != -1)
          return data[i].identity;
      }
      else if (dataProp)
        return data[i].identity;
    }
  },
  searchVersion: function (dataString) {
    var index = dataString.indexOf(this.versionSearchString);
    if (index == -1) return;
    return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
  },
  dataBrowser: [
    {
      string: navigator.userAgent,
      subString: "Chrome",
      identity: "Chrome"
    },
    {   string: navigator.userAgent,
      subString: "OmniWeb",
      versionSearch: "OmniWeb/",
      identity: "OmniWeb"
    },
    {
      string: navigator.vendor,
      subString: "Apple",
      identity: "Safari",
      versionSearch: "Version"
    },
    {
      prop: window.opera,
      identity: "Opera",
      versionSearch: "Version"
    },
    {
      string: navigator.vendor,
      subString: "iCab",
      identity: "iCab"
    },
    {
      string: navigator.vendor,
      subString: "KDE",
      identity: "Konqueror"
    },
    {
      string: navigator.userAgent,
      subString: "Firefox",
      identity: "Firefox"
    },
    {
      string: navigator.vendor,
      subString: "Camino",
      identity: "Camino"
    },
    {   // for newer Netscapes (6+)
      string: navigator.userAgent,
      subString: "Netscape",
      identity: "Netscape"
    },
    {
      string: navigator.userAgent,
      subString: "MSIE",
      identity: "Explorer",
      versionSearch: "MSIE"
    },
    {
      string: navigator.userAgent,
      subString: "Gecko",
      identity: "Mozilla",
      versionSearch: "rv"
    },
    {     // for older Netscapes (4-)
      string: navigator.userAgent,
      subString: "Mozilla",
      identity: "Netscape",
      versionSearch: "Mozilla"
    }
  ],
  dataOS : [
    {
      string: navigator.platform,
      subString: "Win",
      identity: "Windows"
    },
    {
      string: navigator.platform,
      subString: "Mac",
      identity: "Mac"
    },
    {
         string: navigator.userAgent,
         subString: "iPhone",
         identity: "iPhone/iPod"
      },
    {
      string: navigator.platform,
      subString: "Linux",
      identity: "Linux"
    }
  ]

};
browserCheck.init();

function getServerTime(){
var xhr = $.ajax('<?php print constant("SYSURL") ?>/Timer',{}
  ).success(function(data){
    $('#serverTime').html(xhr.responseText);
  }).error(function (){
    $('#serverTime').html('<span class="black heavy notextdeco">&nbsp;&nbsp;Lost connection.</span>');
  });
}

function bash(cmd,now,message){

  if (now===1){
      $('input#terminal').val(cmd).focus();
      $('.terminalResponse').html("");
      bashSubmit();
    } else {
      $('input#terminal').val(cmd).focus();
      $('.terminalResponse').html(message);
    }
}


function bashSubmit() {
          var xhr3 = $.post('<?php print constant("SYSURL") ?>/Pseudoshell',{
          arg:$('input#terminal').val(),
          time:$('#browserTime').html(),
	  su:$("#suButton span").text(),
          browser:browserCheck.browser+" "+browserCheck.version+" "+browserCheck.OS,
          password:$('input#newPassword').attr('password')
          })
          .fail(function() { 
          $('.terminalResponse').html('Parse error.');
          $('input#terminal').focus();
          })
          .done(function() {
          $('.terminalResponse').html(xhr3.responseText);
          $('input#terminal').focus();
          });

}


function makeTime(){

  var serverTime=getServerTime();
  var curr = new Date();

  var y = curr.getFullYear();

  var day = curr.getDate();
  if (day.toString().length < 2) { day = "0" + day;}
  var mo = parseInt(curr.getMonth());
  mo = mo +++ 1;
  if (mo.toString().length < 2) { mo = "0" + mo;}

  var h = curr.getHours();
  if (h.toString().length < 2) { h = "0" + h;}

  var mi = curr.getMinutes();
  if (mi.toString().length < 2) { mi = "0" + mi;}

  var s = curr.getSeconds();
  if (s.toString().length < 2) { s = "0" + s;}

  $('#browserTime').html(+ y+'-'+mo+'-'+day+' '+h+':'+mi+':'+s);

}

// this had to be functionalized because we need to reinitialize mobiscroll when Stop type changes
function timePick() {

 $('.timePick.off.Stop').mobiscroll({
      onBeforeShow: function() {
        var minDateOn = $(this).parent().parent().children('td').children('.on').mobiscroll('getDate');
        $(this).mobiscroll("option",{
        	minDate: new Date(minDateOn)
        } )
      },
      mode: "scroller",
      display: "modal",
      animate: "pop",
      preset: 'time',
      timeFormat: 'HH:ii',
      timeWheels: 'HHii',
      showOnFocus: false,
        onClose: function(){
          $(window).unbind('.dw');
      }
});
  $('.timePick.off.Length').mobiscroll({

      mode: "scroller",
      display: "modal",
      animate: "pop",
      preset: 'time',
      timeFormat: 'HH.ii',
      timeWheels: 'HHii',     
      showOnFocus: false, 
      onClose: function(){
       $(window).unbind('.dw');
      }
});

  $('.timePick.off.Loops').mobiscroll({
      mode: "scroller",
      display: "modal",
      animate: "pop",
      showOnFocus: false,
      wheels:
       [ { 'Loops': { 1: '1', 2: '2', 3: '3',4: '4', 5: '5', 6: '6',7: '7', 8: '8', 9: '9',10: '10', 11: '11', 12: '12',13: '13', 14: '14', 15: '15',16: '16', 17: '17', 18: '18',19: '19', 20: '20', 21: '21',22: '22', 23: '23', 24: '24'} } ],

      onClose: function(){
        $(window).unbind('.dw');
      }
      });
}
$(function() {

$('#noscript').hide();
$('#browserLabel').html(browserCheck.browser+" "+browserCheck.version+" "+browserCheck.OS);

makeTime();
var refreshIntervalId = window.setInterval(makeTime,900);


      $('.EXdelete').on('click', function(){
        $(this).parents('tr').find('input.timePick').val("");
        $(this).parents('tr').find('input.off').addClass('hidden');
      })

  $('ul.dropdown-menu.scheduler li').on('click',function(){
  var label=$(this).find('.scheduleLabel').html();
    $('#scheduleType').html($(this).find('a').html());
    $('#scheduleTypeLabel').html($(this).find('.scheduleIcon').html());
    $('.schedules .interface2').addClass('hidden')
    $('#scheduleTarget').removeClass('hidden');
    $('.schedules').each(function(){
      $(this).find("[data-type='" + label + "']").removeClass('hidden');
    })
  })
})
$(function(){
  $('ul.dropdown-menu.stopper li').on('click',function(){
    $('.stopType span').html($(this).find('a span').html());
    $('table tr td input').removeClass("Stop Length Loops")
    .addClass($(this).find('a span').html());
    $('.timePick.off').mobiscroll('destroy');
    timePick();
    })

/* -------------------------------------------------------------------- mobiscroll stuff */
  $('.datePick').mobiscroll({
    mode: "scroller",
    display: "modal",
    animate: "pop",
    preset: 'date',
    dateFormat: 'yy-mm-dd',
    dateOrder: 'yyMD dd',
    minDate: new Date(),
    showOnFocus: false,
    onClose: function(){
      $(window).unbind('.dw');
    },
      onSelect: function() {
      	$(this).parent().parent().children('td').children('.off').removeClass('hidden');
      }                                      
  });
  $('.timePick.Range').mobiscroll({
  	    onBeforeShow: function() {
        var minDateOn = $(this).parent().parent().children('td').children('.Range').mobiscroll('getDate');
        $(this).mobiscroll("option",{
        	minDate: new Date(minDateOn)
        } )
      },
      mode: "scroller",
      display: "modal",
      animate: "pop",
      preset: 'datetime',
      dateFormat: 'yy-mm-dd ',
      minDate: new Date(),
      dateOrder: 'yyMD dd',
      timeWheels: 'HHii',
      timeFormat: 'HH:ii',
      showOnFocus: false,
      onClose: function(){
       $(window).unbind('.dw');
      },
      onSelect: function() {
      	$(this).parent().parent().children('td').children('.off').removeClass('hidden');
      }
  });

 $('.timePick.on').mobiscroll({
      mode: "scroller",
      display: "modal",
      animate: "pop",
      preset: 'time',
      timeWheels: 'HHii',
      timeFormat: 'HH:ii',
      showOnFocus: false,
      onClose: function(){
       $(window).unbind('.dw');
      },
      onSelect: function() {
      	$(this).parent().parent().children('td').children('.off').removeClass('hidden');
      }
  });

timePick();

// //////////////////////////////////////////////////////////////////////end mobiscroll

// global keymappings
$(document).bind({"keydown": function(e) { 
//13: enter or return
//17: control
//83: s
//81: q
//33 pageup
//34 pagedown

      if (e.keyCode == 13) {
        if ($('input#terminal').is(":focus")) {
            bashSubmit();
          } else if ($('input#newPassword').is(":focus")) {
            $('input#newPassword').blur();
           } else {
             e.preventDefault();
      }
    }
  }
});


             
$('input#newPassword').on('blur', function(){
  // we would use jquery - but the security model prevents type changing.
  //if ()
  bash('passwd',0,'Press enter to permanently change your password.<br>');
   //$('.dropdown-menu').click();

   $(this).attr('password',$(this).val());
    //.val('Change Password');
    var password = document.getElementById('newPassword');
    //password.type = 'text';
    $('input#terminal').focus();
  }).on('click', function(e){
     e.stopPropagation();
  }).on('focus', function(e){
    $(this).addClass("required")
    $(this).val("");
    // lame ass security model from jquery won't work!!!
    //   $(this).type("password");
    var password = document.getElementById('newPassword');
    password.type = 'password';
  })

$('.minicon').on("elementCreated", function(){
    var iconset=this.id, 
    mini=$(this).attr('data-mini');    
    miniMaker(iconset, mini);
})

  $('img.iconset').each(function(){
    var iconset=this.id, 
    icon=$(this).attr('data-icon'), 
    badge=$(this).attr('data-badge');
    iconMaker(iconset, icon);
    badgeMaker(iconset, badge);
  })

  $('body').css('width',screen.width);

  $('#fileTree').fileTree({
  folderEvent: 'click', 
  expandSpeed: 300, 
  collapseSpeed: 150, 
  multiFolder: false 
  }, function(file) { 
    var buttons=buttonbar('file','file');
    $( "#cart ol" ).find( ".placeholder" ).remove();
    $( "<li></li>" )
    .html( buttons + file)
    .appendTo( "#cart ol" )
    .addClass("cart-item");
    activatebuttonbar();
    showPlaylist();
    $("#cart ol").sortable();

  })

    $( "#cart ol" ).droppable({
      greedy: true,
      activeClass: "ui-state-default",
      hoverClass: "ui-state-hover",
      accept: ":not(.ui-sortable-helper)",
      drop: function( event, ui ){
        var buttons=buttonbar(ui.draggable.text(),ui.draggable.text());
        $( this ).find( ".placeholder" ).remove();
        $('.buttons').removeClass('hidden');
        $( "<li></li>" )
        .html( buttons + ui.draggable.html())
        .appendTo( this )
        .addClass("cart-item");
        activatebuttonbar();  
          $(this).removeClass('dragdropTargetHover');
          $(ui.helper).removeClass('dragdropHelperHover');
 
     //       $(this).find('.item-container').html(  buttons + ui.draggable.html() );
 
     //       if ( ui.draggable.parent().is('.drag-container') )
     //          $('.drag-container .item-container').html('Item ' + (++items));
     //       else
               // Defer removing the item after exiting the current call stack
               // so the Draggable widget can complete the drag processing
     //          setTimeout(function () { ui.draggable.remove(); }, 0);
 
         
      },
      activate: function( event,ui ){
          $(this).addClass('dragdropTargetHover');
          $(ui.helper).addClass('dragdropHelperHover');
       showPlaylist();
      },                        
      deactivate: function(event, ui){
          $(this).removeClass('dragdropTargetHover');
          $(ui.helper).removeClass('dragdropHelperHover');
      },
      out: function (event, ui){
      //    $(this).removeClass('dragdropTargetHover');
      //    $(ui.helper).removeClass('dragdropHelperHover');
      }
      
    })

    .sortable({


  items: "li:not(.placeholder)",
  placeholder: "ui-state-highlight",
  containment: "#cart",
  axis: "y",
      sort: function(event, ui) {
        $( this ).removeClass( "ui-state-default" );
        $(this).removeClass('dragdropTargetHover');
        $(ui.helper).removeClass('dragdropHelperHover');
        listCount(1);
      },
      update: function ( e, ui ) {
 
         /* Check if the drag handle is missing */
         if ( ui.item.find('.cart-item').length == 0 ) {
            /* It is so increment the item counter */
           // $('.drag-container .item-container').html('Item ' + (++items));
             
            /* 
                And setup the item so it has a drag handle and
                responds to drag events
            */

  /* ///          ui.item            
               .find('li.file')
              //    .before( $('<div class="drag-handle">') )
               //   .parent()                  
               .draggable({
                
                     cursor: 'move',
                     zIndex: 200,
                     opacity: 0.75,
                     scroll: false,
                     containment: 'window',
                     appendTo: document.body,
                     helper: 'clone',
                      
                  });
             
            /* 
               Reset the containment.  Somehow it breaks when adding
               the external item 
            */
          //  $(this).sortable('option', 'containment', 'parent');
         }
 
      }
    }).disableSelection();

$('#activatePlaylist').on('click', function(e) {

var sortable = $("#cart ol").html(), boxname = $("textarea#box1").val(), scheduleType = $("#scheduleType .scheduleLabel").html(),
target = $("#scheduleTarget input[name=scheduleTarget]:checked").attr('data-target'), filelist="", scheduleData="";

// make the linebroken data
  $( "#cart ol li a.file" ).each(function(){
    if ($(this).attr('rel') != "undefined") {
      filelist=filelist + $(this).attr('rel') + '\n';
    }
  })

// make the linebroken data
$(".interface2."+scheduleType+" tr").each(function(){
	$(this).find('td input').each(function(){
		    scheduleData=scheduleData + $(this).val() + ' ';
	})
//		scheduleData=scheduleData + '\n';
});


//sources.push('Override: '+$('#Override').prop('checked'));
//sources.push('Interrupt: '+$('#Interrupt').prop('checked'));
//var json_text = JSON.stringify(sources, null, 2);
//console.log("filelist\n"+filelist +"\n\n scheduleData\n"+scheduleData );

var xhr2 = $.post('<?php print constant("SYSURL") ?>/Playlistmaker',{
    boxname:boxname,
    filelist:filelist,
    scheduleType:scheduleType,
    scheduleData:scheduleData,
    target:target
    })
    .fail(function() {           
    $('.terminalResponse').html('No connection to server');
    $('input#terminal').val(null).focus();
    })
    .done(function() {
    $('.terminalResponse').html(xhr2.responseText);
    $('input#terminal').val(null).focus();
    });
  })

$('.accordion-body.console').bind({
  click: function(){
  $('input#terminal').focus();
  $('.accordion-body.schedule').collapse('hide');
  $('.accordion-body.playlist').collapse('hide');
  },
  mouseover: function(){
  $('.accordion-body.schedule').collapse('hide');
  $('.accordion-body.playlist').collapse('hide');
  }
})
$('.accordion-body.schedule').bind({
  click: function(){
  $('.accordion-body.console').collapse('hide');
  $('.accordion-body.playlist').collapse('hide');
  },
  mouseover: function(){
  $('.accordion-body.console').collapse('hide');
  $('.accordion-body.playlist').collapse('hide');
  }
})
$('.accordion-body.playlist').bind({
  click: function(){
  $('.accordion-body.schedule').collapse('hide');
  $('.accordion-body.console').collapse('hide');
  },  
  mouseover: function(){
  $('.accordion-body.console').collapse('hide');
  $('.accordion-body.schedule').collapse('hide');
  }
})

});
