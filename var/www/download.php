<?php 

#####################################################
# MODULE: 	download 
# PURPOSE: 	serve files to be downloaded
# USAGE: 	private 
# USED BY:  preview.php
# LICENSE:  MIT or equivalent
# AUTHOR:   denjell
# VERSION:  0.6.1
# PROJECT:	ARTBOX.IO
#####################################################

$file = urldecode($_GET['file']);
$root = "/var/www";
header ("Content-type: octet/stream");
header ("Content-disposition: attachment; filename=".$file.";");
header("Content-Length: ".filesize($root.$file));
readfile($root.$file);
exit;