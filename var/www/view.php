<?php 
// this is the view constructor that is passed responses via the api.

/* 

These are the inherent views:

header.php
menubar.php
  auth.php -> exits if not logged in.


The views we need to construct:

  login /
    name:password 

  configure /
    lan / 
    user /
    language /
    storage /
    security  /
    devices /
      video /
      audio /
  
  media /
    add /
    delete /
    playlist /
    library /
    play /
    loop

  schedule /
    today /
    week /
    month /
    
*/
    switch ($action) {


//  case "Media":
  default:
  require_once constant("SYSPATH").  '/dependencies/filetree/box.php';
  break;
}
/*
?>



<div id="controls2" style="display:none;margin-bottom:4.5em">
    <div class="jumbotron span5">
        <h3>Now playing:</h3>
        <p class="lead">
          <pre>
            No file playing.
          </pre>
        </p>
    </div>
    <div class="jumbotron span5">
        <h3>Debug Info:</h3>
        <p class="lead">
          <pre style="padding:0.5em !important;margin:0 !important;line-height:1em !important">
            <?php 
              print $log_message;
            ?>
          </pre>
        </p>
    </div>
</div>
         <!-- Carousel
    ================================================== -->

    <div id="carousel" class="carousel slide" >
      <div class="carousel-inner" style="margin-top:-4.5em">
        <div class="item active">
          <img src="<?php print constant("SYSURL") ?>/slideshow/slide-01.jpg" alt="" class="img-rounded">
          <div id="" class="container2" >
            <div class="carousel-caption" style="background:#99a !important">
              <h1 style="color:#223">Media File Missing.</h1>
              <p class="lead" style="color:#223">No Description!</p>
              <p class="lead" style="color:#223">No Player</p>
              <p class="lead" style="color:#223">No Meta</p>
              <a class="btn btn-medium btn-primary" href="#previous" data-slide="prev"><i class=" icon-chevron-left icon-white"></i></a>              

              <a class="btn btn-medium btn-primary"  href="#next" data-slide="next" ><i class=" icon-chevron-right icon-white"></i></a> 

            </div>
          </div>
        </div>
  
    </div><!-- /.carousel -->

      <div class="row-fluid marketing" >
        <div class="span6" style="padding:0 2em 0 2em">
          <h2>Newsfeed</h2>
          <h4>Update your Artbox now!</h4>
          <p>We have a critical update for all users of version 1.020b. Visit artbox.</p>
          <hr>

        </div>


      </div>


      <div class="footer">
        <hr style="border-color:#445">

        <p>&copy; Artbox 2013</p>
      </div>

    </div> <!-- /container -->


<div id="mainAlert" class="alert alert-block message" style="display:none"> <!-- .alert-error, .alert-success, .alert-info -->
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h2>Coming Very Soon!</h2>
  <p>
    Artbox!
  </p>
</div>


<div id="modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="background:#ccc;">
  <div class="modal-body" style="height:480px; overflow:hidden">
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true" onclick="$('#modal').modal('toggle')">Close</button>
  </div>


<?php  } ?>
*/