<?php 

#####################################################
# MODULE: 	timer.php 
# PURPOSE: 	update the system time, check the temperature 
#			and show the currently playing file.
# USAGE: 	timer.php
# USED BY:  box.php (via xhr)
# LICENSE:  GPL 2 or equivalent
# AUTHOR:   denjell
# VERSION:  0.4.2
# PROJECT:	ARTBOX.IO
#####################################################

print substr(date('Y-m-d H:i:s O'  , mktime()), 0, -5);
print " - ";
system('cat /sys/class/thermal/thermal_zone0/temp | cut -c1-2');
print "°C - ";
system('cat /home/pi/.artbox/locks/nowplaying.lock');