<?php
//
// jQuery File Tree PHP Connector
//
// Version 1.01b
// Modified by Denjell
//
// Cory S.N. LaViska
// A Beautiful Site (http://abeautifulsite.net/)
// 24 March 2008
//
// History:
//
// 1.01 - updated to work with foreign characters in directory/file names (12 April 2008)
// 1.00 - released (24 March 2008)
//
// Output a list of files for jQuery File Tree
//

 $_POST['dir'] = urldecode($_POST['dir']);
 $_POST['domain'] = urldecode($_POST['domain']);

$root = "/";
if( file_exists($root) ) {
	$files = scandir($root . $_POST['dir']);
	natcasesort($files);
	if( count($files) > 2 ) { /* The 2 accounts for . and .. */
		echo "<ul class=\"jqueryFileTree\" style=\"display: none;\">";
		// All dirs
		foreach( $files as $file ) {
			if( file_exists($root . $_POST['dir'] . $file) && $file != '.' && $file != '..' && is_dir($root . $_POST['dir'] . $file) ) {
				echo "<li class=\"directory collapsed\"><a href=\"#\" rel=\"" . htmlspecialchars($_POST['dir'] . $file) . "/\">" . htmlspecialchars($file) . "</a></li>";
			}
		}
		// All files
		foreach( $files as $file ) {
			if( file_exists($root . $_POST['dir'] . $file) && $file != '.' && $file != '..' && !is_dir($root . $_POST['dir'] . $file) ) {
				$ext = preg_replace('/^.*\./', '', $file);
	switch ($ext) {

	case "mp3":
	case "wav":
	case "ogg":
	case "mp4":	
	case "mpg":
	case "webm":
	case "mov":
	case "jpg":
	case "png":
	case "gif":
	case "tif":	

	$id = $id+1;
		echo '<li class="file ext_'.$ext.'" data-source="'. htmlspecialchars($_POST['dir'] . $file) . '"><a data-toggle="modal" data-target="#modal" class="file link ext_'.$ext.'" href="'. constant("SYSURL").'/Preview/'.$ext.'/?root='.urlencode( htmlspecialchars($root)).'&target='.urlencode( htmlspecialchars($_POST['dir'] . $file )).'" rel="' . htmlspecialchars($root.$_POST['dir'] . $file) . '">' . htmlspecialchars($file) . '</a></li>';
	break;
	case "schedule":
	case "flag":
	case "playlist":
	echo '<li class="file ext_jpg" data-source="'. htmlspecialchars($_POST['dir'] . $file) . '">' . htmlspecialchars($file) . '</li>';
	break;
	default:
	
	break;
	}


			}
		}
		echo "</ul>";	
	}
} else {
	echo "<li>No assets found.</li>";
}
