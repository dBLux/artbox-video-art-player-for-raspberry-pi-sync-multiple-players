#!/bin/sh
### BEGIN INIT INFO
# Provides: omxplayer loop
# Required-Start:
# Required-Stop:
# Should-Stop:
# Default-Start: 2
# Default-Stop:
# Short-Description: Autolooping Player
# Description: Preprocessing before we begin.
### END INIT INFO


case $1 in
  start)
        # mount the USB if available
        /home/pi/.artbox/bin/mountUSB > /dev/null 2>&1

        # check for emergency commando
        /home/pi/.artbox/bin/dsp > /dev/null 2>&1

        # initiate the controller
        # /home/pi/.artbox/bin/scheduler
    ;;
esac


exit 0
